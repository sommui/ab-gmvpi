<?php

/**
 * Copyright 2016 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

namespace LINE\LINEBot\ReportBot;

class Setting
{
    public static function getSetting()
    {
        return [
            'settings' => [
                'displayErrorDetails' => true, // set to false in production

                'logger' => [
                    'name' => 'slim-app',
                    // 'path' => __DIR__ . '/../../../logs/app.log',
                    'path' => 'php://stderr',
                ],

                'bot' => [
                    'channelToken' => getenv('LINE_BOT_CHANNEL_TOKEN') ?: 's+LcbqNdimAKNOwjU1b7ur5jmQMziNGjCH6Pcc4hSSlpHD3hay0FyCGWXa8HBB4dYmFziXm7LPIWBWzzPFgdJWYz9H8FuGkzKVkyfkgarEU8ZobF4O5iyjwfSQUfLEsL/Md30waAkOM9LLz+S+8scAdB04t89/1O/w1cDnyilFU=',
                    'channelSecret' => getenv('LINE_BOT_CHANNEL_SECRET') ?: '74bd97f5d89cadece3feec1342ce4b47',
                ],

                'apiEndpointBase' => getenv('LINEBOT_API_ENDPOINT_BASE'),
            ],
        ];
    }
}
