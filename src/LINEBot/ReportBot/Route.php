<?php

/**
 * Copyright 2016 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

namespace LINE\LINEBot\ReportBot;

use LINE\LINEBot\Constant\HTTPHeader;
use LINE\LINEBot\Event\MessageEvent;
use LINE\LINEBot\Event\MessageEvent\TextMessage;
use LINE\LINEBot\Exception\InvalidEventRequestException;
use LINE\LINEBot\Exception\InvalidSignatureException;
use LINE\LINEBot\ReportBot\Report;

class Route
{
    public function register(\Slim\App $app)
    {   
        $app->post('/callback', function (\Slim\Http\Request $req, \Slim\Http\Response $res) {

            $bot = $this->bot;
            /** @var \Monolog\Logger $logger */
            $logger = $this->logger;

            $signature = $req->getHeader(HTTPHeader::LINE_SIGNATURE);
            if (empty($signature)) {
                return $res->withStatus(400, 'Bad Request');
            }

            // Check request with signature and parse request
            try {
                $events = $bot->parseEventRequest($req->getBody(), $signature[0]);
            } catch (InvalidSignatureException $e) {
                return $res->withStatus(400, 'Invalid signature');
            } catch (InvalidEventRequestException $e) {
                return $res->withStatus(400, "Invalid event request");
            }

            foreach ($events as $event) {
                if (!($event instanceof MessageEvent)) {
                    $logger->info('Non message event has come');
                    continue;
                }

                if (!($event instanceof TextMessage)) {
                    $logger->info('Non text message has come');
                    continue;
                }

                $userMessage = $event->getText();
                switch ($userMessage) {
                    case 'refresh':
                        $report = new Report();
                        $check = $report->refreshFile();
                        if($check > 1) {
                            $msg = "Report is generated... DONE by TESTER";
                        } else {
                            $msg = "file not found";
                        }
                        $botMessage = $msg;
                        $logger->info('Reply text: ' . $botMessage);
                        $resp = $bot->replyText($event->getReplyToken(), $botMessage);
                        break;

                    case 'daily sale report':
                        $report = new Report();
                        $report->generateReport();
                        $report = $report->getReport();
                        $t=time();

                        $multiMessage = new \LINE\LINEBot\MessageBuilder\MultiMessageBuilder();

                        $originalImages = $report["reportA"] . "?time=" . $t;
                        $imageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder($originalImages, $originalImages);
                        $multiMessage->add($imageBuilder);

                        $originalImages =  $report["reportB"] . "?time=" . $t;
                        $imageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder($originalImages, $originalImages);
                        $multiMessage->add($imageBuilder);

                        $originalImages =  $report["reportC"] . "?time=" . $t;
                        $imageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder($originalImages, $originalImages);
                        $multiMessage->add($imageBuilder);

                        $originalImages =  $report["reportD"] . "?time=" . $t;
                        $imageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder($originalImages, $originalImages);
                        $multiMessage->add($imageBuilder);

                        $originalImages =  $report["reportE"] . "?time=" . $t;
                        $imageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder($originalImages, $originalImages);
                        $multiMessage->add($imageBuilder);

                        $resp = $bot->replyMessage($event->getReplyToken(), $multiMessage);
                        $logger->info('MultiMessage: ' . $resp->getHTTPStatus() . ': ' . $resp->getRawBody());

                        $originalImages =  $report["reportF"] . "?time=" . $t;
                        $imageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder($originalImages, $originalImages);
                        $resp = $bot->pushMessage($event->getUserId(), $imageBuilder);

                        $logger->info('PushMessage: ' . $resp->getHTTPStatus() . ': ' . $resp->getRawBody());
                        break;
                }
            }

            $res->write('OK');
            return $res;
        });
    }
}
