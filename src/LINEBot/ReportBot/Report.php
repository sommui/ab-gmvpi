<?php

namespace LINE\LINEBot\ReportBot;
use LINE\LINEBot\ReportBot\Config;
use Johntaa\Arabic\I18N_Arabic;

class Report {

    private $reportA;
    private $reportB;
    private $reportC;
    private $reportD;
    private $reportE;
    private $reportF;

    private $domain;
    private $jsonFile = "report/raw-data.json";
    private $jsonFilePrevious = "report/raw-data-previous.json";

    private $reportAx = [120, 320, 470, 620, 775, 955, 1135, 1310, 1500, 1710, 1920, 2070];
    private $reportAy = [
        [235, 295, 355, 405, 455, 505, 565, 670],
        [725, 775, 835, 895, 945, 995, 1055, 1110],
        [1165, 1225, 1275, 1325, 1385, 1435, 1485, 1545]
    ];

    private $reportBx = [120, 320, 470, 625, 800, 960, 1155, 1360, 1580];
    private $reportBy = [
        [235, 295, 355, 405, 455, 505, 565, 670],
        [725, 775, 835, 895, 945, 995, 1055, 1110],
        [1165, 1225, 1275, 1325, 1385, 1435, 1485, 1545]
    ];

    private $reportCx = [160, 360, 505, 660, 810, 990, 1190, 1350, 1510, 1715, 1920, 2070];
    private $reportCy = [
        [235, 295, 355, 405, 455, 505, 565, 620],
        [670, 725, 775, 835, 895, 945, 995, 1055],
        [1110, 1165, 1225, 1275, 1325, 1385, 1435, 1485],
        [1385, 1440, 1495, 1550, 1605, 1660, 1715, 1770],
        [1770,1825, 1880, 1935, 1990, 2045, 2100, 2155]
    ];

    private $reportDx = [150, 390, 550, 710, 870, 1040, 1200, 1390, 1580];
    private $reportDy = [
        [235, 295, 355, 405, 455, 505, 565, 620],
        [670, 725, 775, 835, 895, 945, 995, 1055],
        [1110, 1165, 1225, 1275, 1325, 1385, 1435, 1485],
        [1385, 1440, 1495, 1550, 1605, 1660, 1715, 1770],
        [1770,1825, 1880, 1935, 1990, 2045, 2100, 2155]
    ];

    private $reportEx = [140,330,470,625,780,955,1155,1335,1500,1710,1905,2065];
    private $reportEy = [230,290,350,410,460,510,560,620,670,720,770,830,880,940,1000];

    private $reportFx = [140,345,495,635,785,940,1130,1360,1580];
    private $reportFy = [230,290,350,410,460,510,560,620,670,720,770,830,890,940,1000];

    private $maxIndexReport = 2;
    private $reportAFeild =  [
        "PRODUCT",
        "TARGET",
        "LAST_YEAR",
        "ESTIMATE_SALES",
        "NET_SALES",
        "DO_NOT_GI",
        "EXPECTED_DO",
        "SALES_EST",
        "SALES_EST_PREVIOUS",
        "ACT_TARGET_PERCENT",
        "EST_TARGET_PERCENT",
        "BACK_ORDER",
        "ACCUM_ACHIVEMENT_PC",
        "ACCUM_TARGET",
        "ACCUM_LAST_YEAR",
        "ACCUM_ACTUAL",
        "ACCUM_EST_CURRENT",
        "ACCUM_EST_PREVIOUS",
        "ACCUM_DIFF_PC",
        "ACCUM_ACT_TARGET_PC",
        "ACCUM_EST_TARGET_PC",
    ];

    public function __construct() {
        // $this->domain =  "https://littlegarden.shop";
        $this->domain =  "https://".$_SERVER['HTTP_HOST'];
    }
    private function generateReportA() {

        $reportName = "report_a.png";
        $templateName = "template/01.png";

        $string = file_get_contents($this->jsonFile);
        $data = json_decode($string, true);

        $string = file_get_contents($this->jsonFilePrevious);
        $dataPrevious = json_decode($string, true);

        $img = imagecreatefrompng($templateName);

        for($i = 0; $i <= $this->maxIndexReport; $i++) {
            if(isset($data["DAILY_SALES_REPORT"][$i]["DISTRICTS"][0]["PRODUCTS"])) {
                $itemList = $data['DAILY_SALES_REPORT'][$i]['DISTRICTS'][0]['PRODUCTS'];
                $itemListPrevious = $dataPrevious['DAILY_SALES_REPORT'][$i]['DISTRICTS'][0]['PRODUCTS'];

                foreach($this->reportAy[$i] as $ky => $ry) {
                    foreach($this->reportAx as $kx => $rx) {
                        $r = true;
                        if($kx == 0) {
                            $r = false;
                        }
                        if($this->reportAFeild[$kx] == "SALES_EST_PREVIOUS") {
                            $list =  $itemListPrevious[$ky];
                            $tx = isset($list["SALES_EST"]) ? $list["SALES_EST"] : "";
                        } else {
                            $list =  $itemList[$ky];
                            $tx = isset($list[$this->reportAFeild[$kx]]) ? $list[$this->reportAFeild[$kx]] : "";
                        }
                        
                        $color = "";
                        if ($this->reportAFeild[$kx] == "ACT_TARGET_PERCENT" || $this->reportAFeild[$kx] == "EST_TARGET_PERCENT"){
                            $percent = (int)str_replace("%", "", $tx);
                            if($percent > 100) {
                                $color = imagecolorallocate($img, 0, 0, 255);
                            }
                        }
                        $bold = false;
                        if($ky == 7) {
                            $bold = true;
                        }
                        $this->drawImage($img, $rx, $ry, $tx , $color, $bold, $r);
                    }
                }
            }
        }

        $color = imagecolorallocate($img, 0, 0, 0);
        $this->drawImage($img, 120, 615, "Online", $color, true, false);
        $asOfDate = "WholeSales Report (All): " . $data['DAILY_SALES_REPORT'][1]['CURRENT_MONTH'];
        $this->drawImageHeader($img, 350, 40, $asOfDate, $color);
        $currentMonth = "Data as of " . $data['DAILY_SALES_REPORT'][1]['AS_OF_DATE'];
        $this->drawImageHeader($img, 1550, 40, $currentMonth, $color);

        imagepng($img, "generate/".$reportName);
        return $this->domain."/public/generate/" . $reportName;

    }
    private function generateReportB() {

        $reportName = "report_b.png";
        $templateName = "template/02.png";

        $string = file_get_contents($this->jsonFile);
        $data = json_decode($string, true);

        $string = file_get_contents($this->jsonFilePrevious);
        $dataPrevious = json_decode($string, true);


        $img = imagecreatefrompng($templateName);
        for($i = 0; $i <= $this->maxIndexReport; $i++) {
            if(isset($data["DAILY_SALES_REPORT"][$i]["DISTRICTS"][0]["PRODUCTS"])) {
                $itemList = $data['DAILY_SALES_REPORT'][$i]['DISTRICTS'][0]['PRODUCTS'];
                $itemListPrevious = $dataPrevious['DAILY_SALES_REPORT'][$i]['DISTRICTS'][0]['PRODUCTS'];
                foreach($this->reportBy[$i] as $ky => $ry) {
                    foreach($this->reportBx as $kx => $rx) {
                        $r = true;
                        if($kx == 0) {
                            $r = false;
                        }
                        if($this->reportAFeild[$kx+12] == "ACCUM_EST_PREVIOUS") {
                            $list =  $itemListPrevious[$ky];
                            $tx = isset($list["ACCUM_EST_CURRENT"]) ? $list["ACCUM_EST_CURRENT"] : "";
                        } else {
                            $list =  $itemList[$ky];
                            $item = $kx+12 == 12 ? $this->reportAFeild[0] : $this->reportAFeild[$kx+12];
                            $tx = isset($list[$item]) ? $list[$item] : "";
                        }
                        $color = "";
                        if ($this->reportAFeild[$kx+12] == "ACCUM_DIFF_PC"){
                            if (strpos($tx, '-') !== false) {
                                $color = imagecolorallocate($img, 255, 0, 0);
                            } else {
                                if($tx == 0) {
                                    $color = imagecolorallocate($img, 0, 0, 0);
                                } else {
                                    $color = imagecolorallocate($img, 0, 0, 255);
                                }
                            }
                        }

                        if ($this->reportAFeild[$kx+12] == "ACCUM_ACT_TARGET_PC" || $this->reportAFeild[$kx+12] == "ACCUM_EST_TARGET_PC"){
                            $percent = (int)str_replace("%", "", $tx);
                            if($percent > 100) {
                                $color = imagecolorallocate($img, 0, 0, 255);
                            }
                        }

                        $bold = false;
                        if($ky == 7) {
                            $bold = true;
                        }
                        $this->drawImage($img, $rx, $ry, $tx, $color, $bold, $r);
                    }
                }
            }
        }
        $color = imagecolorallocate($img, 0, 0, 0);
        $this->drawImage($img, 120, 615, "Online", $color, true, false);
        $asOfDate =  "WholeSales Report (All): " . $data['DAILY_SALES_REPORT'][1]['CURRENT_MONTH'];
        $this->drawImageHeader($img, 130, 40, $asOfDate, $color);
        $currentMonth = "Data as of " . $data['DAILY_SALES_REPORT'][1]['AS_OF_DATE'];
        $this->drawImageHeader($img, 1100, 40, $currentMonth, $color);

        imagepng($img, "generate/".$reportName);
        return $this->domain."/public/generate/" . $reportName;

    }
    private function generateReportC() {
        $reportName = "report_c.png";
        $templateName = "template/03.png";

        $string = file_get_contents($this->jsonFile);
        $data = json_decode($string, true);

        $string = file_get_contents($this->jsonFilePrevious);
        $dataPrevious = json_decode($string, true);

        $img = imagecreatefrompng($templateName);

        $arr = [
            "TARGET_TOTAL_Territory" => 0,
            "LAST_YEAR_TOTAL_Territory" => 0,
            "ESTIMATE_SALES_TOTAL_Territory" => 0,
            "NET_SALES_TOTAL_Territory" => 0,
            "DO_NOT_GI_TOTAL_Territory" => 0,
            "EXPECTED_DO_TOTAL_Territory" => 0,
            "SALES_EST_TOTAL_Territory" => 0,
            "SALES_EST_PREVIOUS_TOTAL_Territory" => 0,
            "ACT_TARGET_PERCENT_TOTAL_Territory" => 0,
            "EST_TARGET_PERCENT_TOTAL_Territory" => 0,
            "BACK_ORDER_TOTAL_Territory" => 0,
        ];

        $arrNone = [
            "TARGET_TOTAL_None_Territory" => 0,
            "LAST_YEAR_TOTAL_None_Territory" => 0,
            "ESTIMATE_SALES_TOTAL_None_Territory" => 0,
            "NET_SALES_TOTAL_None_Territory" => 0,
            "DO_NOT_GI_TOTAL_None_Territory" => 0,
            "EXPECTED_DO_TOTAL_None_Territory" => 0,
            "SALES_EST_TOTAL_None_Territory" => 0,
            "SALES_EST_PREVIOUS_TOTAL_None_Territory" => 0,
            "ACT_TARGET_PERCENT_TOTAL_None_Territory" => 0,
            "EST_TARGET_PERCENT_TOTAL_None_Territory" => 0,
            "BACK_ORDER_TOTAL_None_Territory" => 0,
        ];

        if(isset($data["DAILY_SALES_REPORT"][3]["DISTRICTS"])) {
            $itemList = $data['DAILY_SALES_REPORT'][3]['DISTRICTS'];
            $itemListPrevious = $dataPrevious['DAILY_SALES_REPORT'][3]['DISTRICTS'];
            for($i = 0; $i < 5; $i++) {
                foreach($this->reportCy[$i] as $ky => $ry) {
                    $kyIndex = $ky;
                    foreach($this->reportCx as $kx => $rx) {
                        $r = true;
                        if($kx == 0) {
                            $r = false;
                        }
                        if($i == 4) {
                            $j = $i-1;
                        } else if($i == 3) {
                            $j = $i+1;
                        }else {
                            $j = $i;
                        }
                        if($this->reportAFeild[$kx] == "SALES_EST_PREVIOUS") {
                            $list =  $itemListPrevious[$j]["PRODUCTS"];
                            $tx = isset($list[$kyIndex]["SALES_EST"]) ? $list[$kyIndex]["SALES_EST"] : "";
                        } else {
                            $list =  $itemList[$j]["PRODUCTS"];
                            $tx = isset($list[$kyIndex][$this->reportAFeild[$kx]]) ? $list[$kyIndex][$this->reportAFeild[$kx]] : "";
                        }
                        $color = "";
                        if ($this->reportAFeild[$kx] == "ACT_TARGET_PERCENT" || $this->reportAFeild[$kx] == "EST_TARGET_PERCENT"){
                            $percent = (int)str_replace("%", "", $tx);
                            if($percent > 100) {
                                $color = imagecolorallocate($img, 0, 0, 255);
                            }
                        }
                        $bold = false;
                        if($kyIndex == 7 || ($i == 2 && $kyIndex == 3 ) || ($i == 3 && $kyIndex == 6 ) || ($i == 4 && $kyIndex == 4 )) {
                            $bold = true;
                        }
                        if(($kyIndex == 7 && ($i == 0 || $i == 1)) || $kyIndex == 3 && $i == 2) {
                            $bold = true;
                            if($kx > 0) {
                              $arr[$this->reportAFeild[$kx] . "_TOTAL_Territory"] = $arr[$this->reportAFeild[$kx] . "_TOTAL_Territory"] + (float)str_replace("%","", $tx);
                            }
                        }
                        if(($i == 3 && $kyIndex == 6) || ($i == 4 && $kyIndex == 4)) {
                            $bold = true;
                            if($kx > 0) {
                              $arrNone[$this->reportAFeild[$kx] . "_TOTAL_None_Territory"] = $arrNone[$this->reportAFeild[$kx] . "_TOTAL_None_Territory"] + (float)str_replace("%","", $tx);
                            }
                        }
                        $this->drawImage($img, $rx, $ry, $tx , $color, $bold, $r);
                    }
                }
            }
        }

        $color = imagecolorallocate($img, 0, 0, 0);
        $asOfDate = "WholeSales Report (All): " . $data['DAILY_SALES_REPORT'][3]['CURRENT_MONTH'];
        $this->drawImageHeader($img, 440, 40, $asOfDate, $color);
        $currentMonth = "Data as of " . $data['DAILY_SALES_REPORT'][3]['AS_OF_DATE'];
        $this->drawImageHeader($img, 1550, 40, $currentMonth, $color);
        $this->setTerritory($img);

        $count = 1;
        foreach($arr as $key => $x) {
            $percent = "";
            if($count == 9 || $count == 10) {
                if((int)$x > 100) {
                    $color = imagecolorallocate($img, 0, 0, 255);
                }
                $percent = "%";
            }
            $this->drawImage($img, $this->reportCx[$count] , 1335 , (string)$x.$percent, $color, true);
            $count =  $count + 1;
            $color = imagecolorallocate($img, 0, 0, 0);
        }

        $count = 1;
        $color = imagecolorallocate($img, 255, 255, 255);
        foreach($arrNone as $key => $x) {
            $percent = "";
            if($count == 9 || $count == 10) {
                if((int)$x > 100) {
                    $color = imagecolorallocate($img, 0, 0, 255);
                }
                $percent = "%";
            }
            $this->drawImage($img, $this->reportCx[$count] , 2050 , (string)$x.$percent, $color, true);
            $count =  $count + 1;
            $color = imagecolorallocate($img, 255, 255, 255);
        }

        imagepng($img, "generate/".$reportName);
        return $this->domain."/public/generate/" . $reportName;  
    }
    private function generateReportD() {
        $reportName = "report_d.png";
        $templateName = "template/04.png";

        $string = file_get_contents($this->jsonFile);
        $data = json_decode($string, true);

        $string = file_get_contents($this->jsonFilePrevious);
        $dataPrevious = json_decode($string, true);


        $img = imagecreatefrompng($templateName);

        $arr = [
            "ACCUM_TARGET_TOTAL_Territory" => 0,
            "ACCUM_LAST_YEAR_TOTAL_Territory" => 0,
            "ACCUM_ACTUAL_TOTAL_Territory" => 0,
            "ACCUM_EST_CURRENT_TOTAL_Territory" => 0,
            "ACCUM_EST_PREVIOUS_TOTAL_Territory" => 0,
            "ACCUM_DIFF_PC_TOTAL_Territory" => 0,
            "ACCUM_ACT_TARGET_PC_TOTAL_Territory" => 0,
            "ACCUM_EST_TARGET_PC_TOTAL_Territory" => 0,
        ];

        $arrNone = [
            "ACCUM_TARGET_TOTAL_None_Territory" => 0,
            "ACCUM_LAST_YEAR_TOTAL_None_Territory" => 0,
            "ACCUM_ACTUAL_TOTAL_None_Territory" => 0,
            "ACCUM_EST_CURRENT_TOTAL_None_Territory" => 0,
            "ACCUM_EST_PREVIOUS_TOTAL_None_Territory" => 0,
            "ACCUM_DIFF_PC_TOTAL_None_Territory" => 0,
            "ACCUM_ACT_TARGET_PC_TOTAL_None_Territory" => 0,
            "ACCUM_EST_TARGET_PC_TOTAL_None_Territory" => 0,
        ];

        if(isset($data["DAILY_SALES_REPORT"][3]["DISTRICTS"])) {
            $itemList = $data['DAILY_SALES_REPORT'][3]['DISTRICTS'];
            $itemListPrevious = $dataPrevious['DAILY_SALES_REPORT'][3]['DISTRICTS'];
            for($i = 0; $i < 5; $i++) {
                foreach($this->reportDy[$i] as $ky => $ry) {
                    $kyIndex = $ky;
                    foreach($this->reportDx as $kx => $rx) {
                        $r = true;
                        if($kx == 0) {
                            $r = false;
                        }
                        if($i == 4) {
                            $j = $i-1;
                        } else if($i == 3) {
                            $j = $i+1;
                        }else {
                            $j = $i;
                        }
                        if($this->reportAFeild[$kx+12] == "ACCUM_EST_PREVIOUS") {
                            $list =  $itemListPrevious[$j]["PRODUCTS"];
                            $tx = isset($list[$kyIndex]["ACCUM_EST_CURRENT"]) ? $list[$kyIndex]["ACCUM_EST_CURRENT"] : "";
                        } else {
                            $list =  $itemList[$j]["PRODUCTS"];
                            $item = $kx+12 == 12 ? $this->reportAFeild[0] : $this->reportAFeild[$kx+12];
                            $tx = isset($list[$kyIndex][$item]) ? $list[$kyIndex][$item] : "";
                        }
                        
                        $color = "";
                        if ($this->reportAFeild[$kx+12] == "ACCUM_DIFF_PC"){
                            if (strpos($tx, '-') !== false) {
                                $color = imagecolorallocate($img, 255, 0, 0);
                            } else {
                                if($tx == 0) {
                                    $color = imagecolorallocate($img, 0, 0, 0);
                                } else {
                                    $color = imagecolorallocate($img, 0, 0, 255);
                                }
                            }
                        }
                        if ($this->reportAFeild[$kx+12] == "ACCUM_ACT_TARGET_PC" || $this->reportAFeild[$kx+12] == "ACCUM_EST_TARGET_PC"){
                            $percent = (int)str_replace("%", "", $tx);
                            if($percent > 100) {
                                $color = imagecolorallocate($img, 0, 0, 255);
                            }
                        }
                        $bold = false;
                        if($kyIndex == 7 || ($i == 2 && $kyIndex == 3 ) || ($i == 3 && $kyIndex == 6 ) || ($i == 4 && $kyIndex == 4 )) {
                            $bold = true;
                        }
                        if(($kyIndex == 7 && ($i == 0 || $i == 1)) || $kyIndex == 3 && $i == 2) {
                            $bold = true;
                            if($kx+12 > 12) {
                              $arr[$this->reportAFeild[$kx+12] . "_TOTAL_Territory"] = $arr[$this->reportAFeild[$kx+12] . "_TOTAL_Territory"] + (float)str_replace("%","", $tx);
                            }
                        }
                        if(($i == 3 && $kyIndex == 6) || ($i == 4 && $kyIndex == 4)) {
                            $bold = true;
                            if($kx+12 > 12) {
                              $arrNone[$this->reportAFeild[$kx+12] . "_TOTAL_None_Territory"] = $arrNone[$this->reportAFeild[$kx+12] . "_TOTAL_None_Territory"] + (float)str_replace("%","", $tx);
                            }
                        }
                        $this->drawImage($img, $rx, $ry, $tx, $color, $bold, $r);
                    }
                }
            }
        }

        $color = imagecolorallocate($img, 0, 0, 0);
        $asOfDate = "WholeSales Report (All): " . $data['DAILY_SALES_REPORT'][3]['CURRENT_MONTH'];
        $this->drawImageHeader($img, 160, 40, $asOfDate, $color);
        $currentMonth = "Data as of " . $data['DAILY_SALES_REPORT'][3]['AS_OF_DATE'];
        $this->drawImageHeader($img, 1100, 40, $currentMonth, $color);
        
        $this->setTerritory($img);

        $count = 1;
        foreach($arr as $key => $x) {
            $percent = "";
            if($count == 6 || $count == 7 || $count == 8) {
                $percent = "%";
            }
            if(($count == 6 || $count == 7 || $count == 8)) {
                if ((float)$x < 100 && $count == 6) {
                    $color = imagecolorallocate($img, 255, 0, 0);
                } else {
                    $color = imagecolorallocate($img, 0, 0, 255);
                }
            }
            $this->drawImage($img, $this->reportDx[$count] , 1335 , (string)$x.$percent, $color, true);
            $count =  $count + 1;
            $color = imagecolorallocate($img, 0, 0, 0);
        }

        $count = 1;
        $color = imagecolorallocate($img, 255, 255, 255);
        foreach($arrNone as $key => $x) {
            $percent = "";
            if($count == 6 || $count == 7 || $count == 8) {
                $percent = "%";
            }
            if(($count == 6 || $count == 7 || $count == 8) && (float)$x > 100) {
                $color = imagecolorallocate($img, 0, 0, 255);
            }
            $this->drawImage($img, $this->reportDx[$count] , 2050 , (string)$arrNone[$key].$percent, $color, true);
            $count =  $count + 1;
            $color = imagecolorallocate($img, 255, 255, 255);
        }


        imagepng($img, "generate/".$reportName);
        return $this->domain."/public/generate/" . $reportName;
    }
    private function generateReportE() {
        $reportName = "report_e.png";
        $templateName = "template/05.png";

        $string = file_get_contents($this->jsonFile);
        $data = json_decode($string, true);

        $string = file_get_contents($this->jsonFilePrevious);
        $dataPrevious = json_decode($string, true);

        $img = imagecreatefrompng($templateName);
        
        $arr = [
            "TARGET_TOTAL" => 0,
            "LAST_YEAR_TOTAL" => 0,
            "ESTIMATE_SALES_TOTAL" => 0,
            "NET_SALES_TOTAL" => 0,
            "DO_NOT_GI_TOTAL" => 0,
            "EXPECTED_DO_TOTAL" => 0,
            "SALES_EST_TOTAL" => 0,
            "SALES_EST_PREVIOUS_TOTAL" => 0,
            "ACT_TARGET_PERCENT_TOTAL" => 0,
            "EST_TARGET_PERCENT_TOTAL" => 0,
            "BACK_ORDER_TOTAL" => 0,
        ];
       
        $itemList = $data['DAILY_SALES_REPORT'][4]['DISTRICTS'][0]['PRODUCTS'];
        $itemList2 = $data['DAILY_SALES_REPORT'][4]['DISTRICTS'][1]['PRODUCTS'];

        $itemListPrevious1 = $dataPrevious['DAILY_SALES_REPORT'][4]['DISTRICTS'][0]['PRODUCTS'];
        $itemListPrevious2 = $dataPrevious['DAILY_SALES_REPORT'][4]['DISTRICTS'][1]['PRODUCTS'];

        $items = array_merge($itemList, $itemList2);
        $itemsPrevious = array_merge($itemListPrevious1, $itemListPrevious2);
        foreach($this->reportEy as $ky => $y) {
            $kyIndex = $ky;
            foreach($this->reportEx as $kx => $x) {
                $r = true;
                if($kx == 0) {
                    $r = false;
                }
                if($this->reportAFeild[$kx] == "SALES_EST_PREVIOUS") {
                    $list =  $itemsPrevious[$kyIndex];
                    $tx = isset($list["SALES_EST"]) ? $list["SALES_EST"] : "";
                } else {
                    $list =  $items[$kyIndex];
                    $tx = isset($list[$this->reportAFeild[$kx]]) ? $list[$this->reportAFeild[$kx]] : "";
                }

                $color = "";
                if ($this->reportAFeild[$kx] == "ACT_TARGET_PERCENT" || $this->reportAFeild[$kx] == "EST_TARGET_PERCENT"){
                    $percent = (int)str_replace("%", "", $tx);
                    if($percent > 100) {
                        $color = imagecolorallocate($img, 0, 0, 255);
                    }
                }
                $bold = false;
                if($kyIndex == 7 || $kyIndex == 14) {
                    $bold = true;
                    if($kx > 0) {
                      $arr[$this->reportAFeild[$kx] . "_TOTAL"] = $arr[$this->reportAFeild[$kx] . "_TOTAL"] + (float)str_replace("%","", $tx);
                    }
                }
                $this->drawImage($img, $x, $y, $tx, $color, $bold,  $r);
            }
        }

        $color = imagecolorallocate($img, 0, 0, 0);
        $asOfDate = "WholeSales Report (All): " . $data['DAILY_SALES_REPORT'][4]['CURRENT_MONTH'];
        $this->drawImageHeader($img, 400, 40, $asOfDate, $color);
        $currentMonth = "Data as of " . $data['DAILY_SALES_REPORT'][4]['AS_OF_DATE'];
        $this->drawImageHeader($img, 1550, 40, $currentMonth, $color);

        $this->drawImage($img, 185 , 1060 , "GRAND TOTAL", $color, true);
        $count = 1;
        foreach($arr as $key => $x) {
            $percent = "";
            if($count == 9 || $count == 10) {
                if((int)$x > 100) {
                    $color = imagecolorallocate($img, 0, 0, 255);
                }
                $percent = "%";
            }
            $this->drawImage($img, $this->reportEx[$count] , 1060 , (string)$x.$percent, $color, true);
            $count =  $count + 1;
            $color = imagecolorallocate($img, 0, 0, 0);
        }

        imagepng($img, "generate/".$reportName);
        return $this->domain."/public/generate/" . $reportName;
    }
    private function generateReportF() {
        $reportName = "report_f.png";
        $templateName = "template/06.png";

        $string = file_get_contents($this->jsonFile);
        $data = json_decode($string, true);

        $string = file_get_contents($this->jsonFilePrevious);
        $dataPrevious = json_decode($string, true);

        $img = imagecreatefrompng($templateName);

        $arr = [
            "ACCUM_TARGET_TOTAL" => 0,
            "ACCUM_LAST_YEAR_TOTAL" => 0,
            "ACCUM_ACTUAL_TOTAL" => 0,
            "ACCUM_EST_CURRENT_TOTAL" => 0,
            "ACCUM_EST_PREVIOUS_TOTAL" => 0,
            "ACCUM_DIFF_PC_TOTAL" => 0,
            "ACCUM_ACT_TARGET_PC_TOTAL" => 0,
            "ACCUM_EST_TARGET_PC_TOTAL" => 0,
        ];

        $itemList = $data['DAILY_SALES_REPORT'][4]['DISTRICTS'][0]['PRODUCTS'];
        $itemList2 = $data['DAILY_SALES_REPORT'][4]['DISTRICTS'][1]['PRODUCTS'];

        $itemListPrevious1 = $dataPrevious['DAILY_SALES_REPORT'][4]['DISTRICTS'][0]['PRODUCTS'];
        $itemListPrevious2 = $dataPrevious['DAILY_SALES_REPORT'][4]['DISTRICTS'][1]['PRODUCTS'];

        $items = array_merge($itemList, $itemList2);
        $itemsPrevious = array_merge($itemListPrevious1, $itemListPrevious2);

        foreach($this->reportFy as $ky => $y) {
            $kyIndex = $ky;
            foreach($this->reportFx as $kx => $x) {
                $r = true;
                if($kx == 0) {
                    $r = false;
                }
                if($kx != 0) {
                    $key = $kx + 12;
                } else {
                    $key = $kx;
                }

                if($this->reportAFeild[$kx+12] == "ACCUM_EST_PREVIOUS") {
                    $list =  $itemsPrevious[$kyIndex];
                    $tx = isset($list["ACCUM_EST_CURRENT"]) ? $list["ACCUM_EST_CURRENT"] : "";
                } else {                 
                    $list =  $items[$kyIndex];
                    $tx = isset($list[$this->reportAFeild[$key]]) ? $list[$this->reportAFeild[$key]] : "";
                }

                $color = "";
                if ($this->reportAFeild[$kx+12] == "ACCUM_DIFF_PC"){
                    if (strpos($tx, '-') !== false) {
                        $color = imagecolorallocate($img, 255, 0, 0);
                    } else {
                        if($tx == 0) {
                            $color = imagecolorallocate($img, 0, 0, 0);
                        } else {
                            $color = imagecolorallocate($img, 0, 0, 255);
                        }
                    }
                }


                if ($this->reportAFeild[$kx+12] == "ACCUM_ACT_TARGET_PC" || $this->reportAFeild[$kx+12] == "ACCUM_EST_TARGET_PC"){
                    $percent = (int)str_replace("%", "", $tx);
                    if($percent > 100) {
                        $color = imagecolorallocate($img, 0, 0, 255);
                    }
                }


                $bold = false;
                if($kyIndex == 7 || $kyIndex == 14) {
                    $bold = true;
                    if($kx > 0) {
                        $arr[$this->reportAFeild[$kx+12] . "_TOTAL"] = $arr[$this->reportAFeild[$kx+12] . "_TOTAL"] + (float)str_replace("%","", $tx);
                    }
                }
                $this->drawImage($img, $x, $y, $tx, $color, $bold, $r);
            }
        }

        $color = imagecolorallocate($img, 0, 0, 0);
        $asOfDate = "WholeSales Report (All): " . $data['DAILY_SALES_REPORT'][4]['CURRENT_MONTH'];
        $this->drawImageHeader($img, 130, 40, $asOfDate, $color);
        $currentMonth = "Data as of " .  $data['DAILY_SALES_REPORT'][4]['AS_OF_DATE'];
        $this->drawImageHeader($img, 1050, 40, $currentMonth, $color);

        $this->drawImage($img, 185 , 1060 , "GRAND TOTAL", $color, true);
        $count = 1;
        foreach($arr as $key => $x) {
            $percent = "";
            if($count == 6 || $count == 7 || $count == 8) {
                if ((float)$x < 100 && $count == 6) {
                    $color = imagecolorallocate($img, 255, 0, 0);
                } else {
                    $color = imagecolorallocate($img, 0, 0, 255);
                }
                $percent = "%";
            }
            $this->drawImage($img, $this->reportFx[$count] , 1060 , (string)$x.$percent, $color, true);
            $count =  $count + 1;
            $color = imagecolorallocate($img, 0, 0, 0);
        }

        imagepng($img, "generate/".$reportName);
        return $this->domain."/public/generate/" . $reportName;
    }
    private function setTerritory($img) {
        $this->drawImageHeader($img, 40, 1335, "Total-Territory" , imagecolorallocate($img, 0, 0, 0));
        $this->drawImageHeader($img, 10, 2050, "Total-None Territory" , imagecolorallocate($img, 255, 255, 255));
    }
    public function getReport() {
        return [
            "reportA" => $this->reportA,
            "reportB" => $this->reportB,
            "reportC" => $this->reportC,
            "reportD" => $this->reportD,
            "reportE" => $this->reportE,
            "reportF" => $this->reportF,
        ];
     }

     private static function  drawImageHeader($img, $x, $y, $txt, $color) {
        $fontfile = "font/arial-bold.ttf";
        imagettftext($img , 18 , 0 , $x , $y , $color , $fontfile , $txt == "0" ? "0.0" : $txt);
    }
    private static function  drawImage($img, $x, $y, $txt, $color = '', $bold = false, $right = true) {
        if($color == '') {
            $color = imagecolorallocate($img, 0, 0, 0);
        }
        if($bold) {
            $fontfile = "font/arial-bold.ttf";
        } else {
            $fontfile = "font/arial.ttf";
        }
        $txt = $txt == "0" ? "0.0" : $txt;
        $fontSize = 18;
        $dimensions = imagettfbbox($fontSize, 0, $fontfile, $txt);
        $textWidth = abs($dimensions[4] - $dimensions[0]);
        if($right) {
            $xx = $x - $textWidth;
        } else {
            $xx = $x - ($textWidth / 2);
        }

        imagettftext($img , $fontSize, 0, $xx , $y , $color , $fontfile , $txt);

    }
    private static function utf8Strrev($str){

        preg_match_all('/./us', $str, $ar);
     
        return join('',array_reverse($ar[0]));
     
     }

    public function generateReport() {
       $this->reportA = $this->generateReportA();
       $this->reportB = $this->generateReportB();
       $this->reportC = $this->generateReportC();
       $this->reportD = $this->generateReportD();
       $this->reportE = $this->generateReportE();
       $this->reportF = $this->generateReportF();
    }
    public function refreshFile() {
        $config = new Config();
        $ftp_server = $config->ftp_server;
        $ftp_username =  $config->ftp_username;
        $ftp_userpass =  $config->ftp_userpass;
        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
        $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
        $file_list = ftp_nlist($ftp_conn, "./LINE_INTERFACE/");
        $file_lists = array();
        foreach($file_list as $row) {
            if (strpos($row, 'DailySales') !== false) {
              array_push($file_lists, $row); 
            }
        }
        $local_file = "report/raw-data.json";
        $local_file_previous = "report/raw-data-previous.json";
        if(count($file_lists) < 2) {
            return count($file_lists);
        } else {
            $server_file = $file_lists[count($file_lists)-1];
            $server_file_previous = $file_lists[count($file_lists)-2];
            ftp_get($ftp_conn, $local_file, $server_file, FTP_ASCII);
            ftp_get($ftp_conn, $local_file_previous, $server_file_previous, FTP_ASCII);
            ftp_close($ftp_conn);
            return count($file_lists);
        }
    }
}

?>