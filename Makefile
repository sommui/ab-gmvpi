.PHONY: logs
logs:
	heroku logs --tail --app line-report

.PHONY: bash
bash:
	heroku run bash

.PHONY: deploy
deploy:
	git push heroku master